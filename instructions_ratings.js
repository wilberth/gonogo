var r1 = "<h2>Rating task</h2>"+ //Only group 'a' (even ppn) get r1
		 "<p>In the next part of the experiment, you will be shown two rewards, one by one.</p>"+
		 "<p>For each reward, please use the slider to indicate how much you would like to receive the reward.</p>"+
		 "<p>Don't compare the two rewards, just evaluate each reward on its own.</p>"+
		 "<p>We are interested to learn about your opinion. Thus, are no right or wrong answers.<p>"

var r2a = "<p>Next, you will be shown several gems, one by one.</p>"+
		 "<p>For each gem, please use the slider to indicate how much you like the gem.</p>"+
		 "<p>Don't compare the gems, just evaluate each gem on its own.</p>"+
		 "<p>We are interested to learn about your opinion. Thus, are no right or wrong answers.</p>"

var r2b = "<h2>Rating task</h2>"+
		  "<p>In the next part of the experiment, you will be shown several gems, one by one.</p>"+
		 "<p>For each gem, please use the slider to indicate how much you like the gem.</p>"+
		  "<p>Don't compare the gems, just evaluate each gem on its own.</p>"+
		 "<p>We are interested to learn about your opinion. Thus, are no right or wrong answers.</p>"

var r3 = "<h2>Rating task</h2>"+ //Only group 'b' (odd ppn) get r3
		 "<p>Now, you will be shown two rewards, one by one.</p>"+
		 "<p>For each reward, please use the slider to indicate how much you would like to receive the reward.</p>"+
		 "<p>Don't compare the two rewards, just evaluate each reward on its own.</p>"+
		 "<p>We are interested to learn about your opinion. Thus, are no right or wrong answers.<p>"

var r4a = "<h2>Rating task</h2>"+
		  "<p>Now, similar to the start of the experiment, you will again be shown several gems, one by one.</p>"+
		 "<p>For each gem, please use the slider to indicate how much you like the gem.</p>"+
		  "<p>Don't compare the gems, just evaluate each gem on its own.</p>"+
		 "<p>We are interested to learn about your opinion. Thus, are no right or wrong answers.</p>"

var r4b = "<p>Next, similar to the start of the experiment, you will again be shown several gems, one by one.</p>"+
		 "<p>For each gem, please use the slider to indicate how much you like the gem.</p>"+
		  "<p>Don't compare the gems, just evaluate each gem on its own.</p>"+
		 "<p>We are interested to learn about your opinion. Thus, are no right or wrong answers.</p>"

var r5 = "<h2>Gem Quiz</h2>"+
		 "<p>Next, there will be a short quiz about the gem game you did earlier.</p>"+
		 "<p>Try to answer these questions correctly.</p>"

var r6 = "<p>You have hereby come to the end of the experiment.</p>"+
		"<p>Thank you for your participation!</p>"+
	 	"<p>Since you participated in a pilot version of the experiment, we would like to ask you some questions about your experience during the experiment."

var r7 = "<p>This is the end of the experiment.</p>"+
		"<p>Thanks again for your participation!</p>"




		 