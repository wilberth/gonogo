var p0_i0 = "<h2>Gem Game</h2>"+
			"<br></br>"+
			"<p>You will now be playing a gem game.</p>"+
			"The better you perform during the game, the higher the chance that you will receive an additional <strong>monetary bonus</strong>.</p>"+
			"<p><strong>Thus, read all of the following instructions carefully!</strong> </p>"

var p0_i3 = "<span class='centerImage'><p>During the gem game, you will be shown different gems, such as this one: </p>"+
			"<p style='text-align:center;'><img src='image/A1_30.png'></p>"+
			"<p>Each gem will be displayed on the screen for a short duration.</p>"+
			"<p>Every time you see a gem appear on the screen, you need to quickly choose whether to <strong>collect or not collect the gem</strong>.</p>"+
			"<p>You collect a gem by <strong>pressing the spacebar</strong> before the gem disappears, or you can leave it behind (not collect it) by <strong>not doing anything</strong> until it disappears.</p></span>"+
			"<p>If you collect the gem, try to do this <strong>as quickly as possible!</strong></p>"

var p0_i4 = "<p>In the game, your choice to collect or not collect the gem can result in one of three possible outcomes.</p>"+
	 		"<p>The first possible outcome is that you do not win a reward. This is indicated as <strong>00000.</strong></p>"+
			"<p>Alternatively, you might win a reward. There are two possible rewards, which differ in their reward <strong>magnitude</strong>, and in their <strong>time of delivery</strong>.</p>"+
			"<p>For instance, one of them could be <strong>&euro;10 today</strong>, and the other could be <strong>&euro;15 in 50 days</strong>.</p>"

var p0_i5 = "<p>The game consists of 4 different gems. Each gem has <strong>one correct response: collecting or not collecting it</strong>.</p>"+
	 		"<p>Following the example mentioned above, this might be as follows:</p>"+
			"<br></br>"+
			"<p>Gem 1: Collect to win &euro;10 today</p>"+
			"<p>Gem 2: Do not collect to win &euro;10 today</p>"+
			"<p>Gem 3: Collect to win &euro;15 in 50 days</p>"+
			"<p>Gem 4: Do not collect to win &euro;15 in 50 days</p>"+
			"<br></br>"+
			"<p>These are just examples. Your task will be to learn what the correct response is for each gem.</p>"+
			"<p>You can learn this by trying out both responses and seeing what the outcome is. So pay attention during the task!"

var p0_i5b = "<p>To make the task a little harder, you may sometimes get no reward, even if you gave the correct response. Or, you may get a reward, even if you did not give the correct response.</p>"+
             "<p>Nevertheless, keep in mind that the correct response will earn you a reward <strong>most of the time.</strong></p>"

var p0_i5c = "<p>The better you play the gem game, the higher the chance that you will be actually paid out in real one of the rewards you obtained in the game.</p>"+
			"<p>We will determine how well you played by the number of correct responses, and by the speed with which you (correctly) collected the gems.</p>"+
			"<p>If you are paid out one of the rewards after the game, this will be done exactly in the way it was stated during the game."+
			"<p>For instance, you may receive &euro;15 in 15 days from now.</p>"

var p0_i6 = "<p>Let's practice this for an example gem!</p>"+
			"<span class='centerImage'><p>You will notice that you need to <strong>collect</strong> this gem in order to win <strong>&euro;10 today</strong> most of the time.</p>"+
			"<p style='text-align:center;'><img src='image/A2_30.png' ></p>"+
			"<p>Remember that a gem can be collected by pressing the space bar.<p>"+
			"<p>Let's try it! You will now be presented with this gem 5 times.<p>"+
			"<p>Press any key when you are ready to start.</p>" 

var p0_i7a = "<p>It seems as if you had some trouble with the practice trials.</p>" +
			 "<p>Let's try this again.</p>"+
			 "<p>Press any key to continue</p>"

var p0_i7b = "<p style='text-align:center;'><img src='image/A2_30.png'</p>"+
			 "<p>Well done!</p>" +
			 "<p>You probably noticed that you won &euro;10 today most of the time by collecting the gem.</p>" +
			 "<p>Perhaps, you sometimes did not get the reward despite having collected the gem. Or, you sometimes got the reward even if you did not collect the gem.</p>" +
			 "<p>Nevertheless, collecting it resulted in the reward most often.</p>" +
			 "<p>Thus, <strong> collecting was the correct response for this gem.</strong></p>" 

var p0_i8 = "<p>This was just one example gem.</p>"+
			"<p>For other gems, you might receive a different reward, and/or you should maybe not collect it to receive a reward.<p>"+
			"<p>During the game, you will need to figure out yourself what to do with each gem.</p>"+
			"<p>Which response (collecting or not collecting) is correct for each gem does not change during the game.</p>"+
			"<p>The game is quite hard, so try to explore all options. At first you might be confused, but don't worry, you will get plenty of chances to learn the game!</p>"

var p0_i9 = "<p>These are the four gems that you will encounter during the game.</p>"+
			"<table class = 'center'><tr><td><img width = 150 src = 'image/B1_30.png'></td><td><img width = 150 src = 'image/B2_30.png'></td><td><img width = 150 src = 'image/B3_30.png'></td><td><img width = 150 src = 'image/B4_30.png'></td></tr></table>"

var p0_i10 = "<p>As explained before, paying attention to the outcomes will help you to learn the task.</p>"+
			  "<p>Once every so often, you will be shown one of the gems, and asked which outcome you receive if you give a correct response for this gem.</p>"+
			  "<p>Try to answers these questions correctly!</p>"
			
var p0_i11 = "<span class = 'centerImage'><p><strong>Look out:</strong> every now and then, you will see this <strong>target</strong> on the screen.</p>"+
	 		 "<p style = 'text-align:center;'><img src='image/target_small.png'></p>"+
			 "<p>In these trials, you should <strong>wait</strong> until the target disappears and then <strong>press</strong> the spacebar as quickly as possible!</p>"+
			 "<p>So do not press immediately once you see the target, but wait until it disappears and <em>then</em> press as quickly as possible!</p>"+
			 "<p>These trials will allow us to see whether you are still paying attention to the game.</p>"

var p0_i12 = "<p>The game will be divided in 4 blocks, separated by short breaks.</p>"

var p0_i13 = "<p>Are you ready for the game?</p>"+
			 "<p>If anything is unclear, you can go back to reread the instructions.</p>"+
			 "<p>If everything is clear, you can continue.</p>"

var p0_i14 = "<p><strong>Good luck!</strong></p>"+
		     "<p>Press any key to start the game.</p>"

var endgg = "<p>You have completed the gem game.</p>"+
			"<p>You have won &euro;X.</p>"+
			"<p>Click on 'Next' to continue.</p>"




			
			





			  

	
	
	