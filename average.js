//return average k where switch takes place from ss to ll
// ks: list of k values (unsorted)
// choices, 0: ss, 1: ll
// [order]: order this list ascending, may contain equal values, use k if absent
ks = [0.00016, 0.006, 0.006, 0.25, 0.041, 0.0004, 0.1, 0.1, 0.00016, 0.006, 0.25, 0.001, 0.00016, 0.041, 0.0025, 0.0025, 0.0004, 0.016, 0.1, 0.0004, 0.016, 0.0025, 0.041, 0.001, 0.016, 0.001, 0.25]

choicesConsistent   = [0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1]
//choicesInconsistent = [1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1]
choicesInconsistent = [0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1]
choicesAll          = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
choicesNone         = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

Array.prototype.sum = Array.prototype.sum || function() {
	return this.reduce(function(sum, a) { return sum + Number(a) }, 0)
}
Array.prototype.average = Array.prototype.average || function() {
	return this.sum() / (this.length || 1)
}
Array.prototype.geomean = function() {	
	return this.reduce(function(a,b){ return a*b }) ** (1/this.length)
}

Array.prototype.harmean = function() {
	return this.length / this.map(a => 1/a).sum() 
}

// change this to the function to be used: average/ geomean/ harmean
Array.prototype.mymean = Array.prototype.average

function getK(ks, choices){
	// k values, and wether the choices was for LL or not
	// note that ks and choices must be arrays of equal length with a multiple of 3 values
	// sort k-values
	let kc = ks.map((k, i) => { return [k, choices[i]] })
	kc.sort( (a, b) => (a[0]-b[0]) ) // sort by k
	console.log( kc )
	
	// group each set of 3 k-values
	let kcr = []
	for(let i=0; i<kc.length/3; i++){
		kcr[i] = [(kc[3*i][0] + kc[3*i+1][0] +kc[3*i+2][0])/3, (kc[3*i][1] + kc[3*i+1][1] +kc[3*i+2][1])/3]
		if(Math.max(kc[3*i][0], kc[3*i+1][0], kc[3*i+2][0]) - Math.min(kc[3*i][0], kc[3*i+1][0], kc[3*i+2][0])
				> 0.01 * kcr[i][0]) // 1% diffrence in 'equal' k-values allowed
			console.error(`not all values in group ${i} appear equal: ` [kc[3*i][0], kc[3*i+1][0], kc[3*i+2][0]])
	}
		
	console.log( kcr )
	let switchValue = []
	// with increasing k (interest) we expect precisely one switch from now (5) to later (6)
	if(!choices.some((a)=>a>0.5)){
		console.log("no switch, all SS")
		switchValue.push([kcr[kcr.length-1][0], kc.length]) // highest k, completely conformant
	} else if(choices.every((a)=>a>0.5)){
		console.log("no switch, all LL")
		switchValue.push([kcr[0][0], kc.length]) // lowest k, completely conformant
	} else
		for(let i=1; i<kcr.length; i++) // over de laatste 8 van de negen k waardes
			if(kcr[i-1][1]<0.5 && kcr[i][1]>0.5){ // als de keuze hier wel en bij de vorige niet in meerderheid LL is
				console.log(`switch between k({${i-1}): ${kcr[i-1][0]} and k({${i}): ${kcr[i][0]}}`)
				// number of conformant choices is number of zeros before 
				let nConformant = 0
				for(let j=0; j<kc.length; j++){
					if( (j/3 < i && kc[j][1]==0) || (j/3 >= i && kc[j][1]==1) )
						nConformant++
				}
				switchValue.push([[kcr[i-1][0], kcr[i][0]].mymean(), nConformant]) // arithmetic mean of k values
			}
	console.log(switchValue)
	if(switchValue.length==1)
		return switchValue[0][0]
	else {
		let m = Math.max(...switchValue.map((a) => a[1])) // maximum number of conformant values
		let k = switchValue.filter(a => a[1]==m).map(a => a[0]).mymean() // average if ks at maximum conformant
		console.log("multiple max at:", k)
		return k
	}
}


