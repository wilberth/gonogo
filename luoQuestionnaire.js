/*
Array.prototype.sum = Array.prototype.sum || function() {
  return this.reduce(function(sum, a) { return sum + Number(a) }, 0)
}
Array.prototype.average = Array.prototype.average || function() {
  return this.sum() / (this.length || 1)
}
Array.prototype.geomean = function() {
  return this.reduce(function(a,b){ return a*b }) ** (1/this.length)
}
*/
// Value, Amount and delay values in Luo questionnaire
vad = [
[54, 55, 117],
[55, 75, 61],
[19, 25, 53],
[31, 85, 7],
[14, 25, 19],
[47, 50, 160],
[15, 35, 13],
[25, 60, 14],
[78, 80, 162],
[40, 55, 62],
[11, 30, 7],
[67, 75, 119],
[34, 35, 186],
[27, 50, 21],
[69, 85, 157],
[49, 60, 89],
[80, 85, 157],
[24, 35, 29],
[33, 80, 14],
[28, 30, 179],
[34, 50, 30],
[25, 30, 80],
[41, 75, 20],
[54, 60, 111],
[54, 80, 30],
[22, 25, 136],
[20, 55, 7],
]

function initLuoQuestionnaire(){
	let table = document.querySelector("table")
	for(let i=0; i<vad.length; i++) { 
		let a = vad[i] 
		table.innerHTML += `
		<div style = "overflow:scroll">
		<tr>
			<td class = now>&euro;${vad[i][0]} today</td>
			<td><input id=n${i} name=r${i} type=radio required></td>
			<td> or </td>
			<td><input id=l${i} name=r${i} type=radio required></td>
			<td class = later>&euro;${vad[i][1]} in ${vad[i][2]} days</td>
		</tr>
		</div>
		`
	}
	//document.getElementById(`submit`).addEventListener("click", () => {
	document.getElementById(`form`).addEventListener("submit", () => {
		vad = vad.map((a, i) => ([ a[0], a[1], a[2], i, (a[1]/a[0]-1)/a[2], 
			Number(document.getElementById(`l${i}`).checked) ])) // add i, k, choice
		window.k = getK(vad.map(a => a[4]), vad.map(a => a[5]))
		/*
		let switchValue = []
		// add index, k (inverse halftime), and responses to vad array
		vad = vad.map((a, i) => [ a[0], a[1], a[2], i, (a[1]/a[0]-1)/a[2],
			document.getElementById(`n${i}`).checked, document.getElementById(`l${i}`).checked ]) 
		vad.sort( (a, b) => (a[4]-b[4]) ) // sort by k
		console.log( vad )
		// with increasing k (interest) we expect precisely one switch from now (5) to later (6)
		if(vad.some(a => !a[5] && !a[6])){
			console.log("not all filled, should be provented by browser")
			switchValue.push(0)
		} else if(vad.every(a => a[5])){
			console.log("no switch, all now")
			switchValue.push(vad[vad.length-1][4]) // highest k
		} else if(vad.every(a => a[6])){
			console.log("no switch, all later")
			switchValue.push(vad[0][4]) // lowest k
		} else
			for(let i=1; i<vad.length; i++)
				if(vad[i-1][5] && vad[i][6]){
					console.log(`switch between q(${vad[i-1][3]}): ${vad[i-1][4]} and q(${vad[i][3]}): ${vad[i][4]}`)
					// print indifference value for LL=28 and average it, rather than k
					switchValue.push((vad[i-1][4] + vad[i-1][4])/2) // arithmetic mean of k values
				}
		window.k = switchValue.geomean()
		*/
		// indifference value for ll = 28 at 120 days
		window.indifferenceValue = 28/(1+window.k*120)
		console.log(`k: ${window.k}, indifference value for €28 in 120 days: € ${window.indifferenceValue}`)
		document.getElementById("submit2").click()
	}, false)
}
