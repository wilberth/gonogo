//Pre-gem game titrator 

var intro1 = "<p>Welcome to the experiment!</p>"+
			"<p>In this experiment, you will be doing several tasks, such as a choice task, and a gem game.</p>"+
			"<p>The experiment will take approximately 20 minutes to complete.</p>"

var intro2 = "<p>To decrease distractions, please put your smartphone away and do not open your smartphone or other web pages on your computer during the experiment.</p>"+
			 "<p>Thank you very much.</p>"


var i0 = "<h2>Choice task</h2>"+
	 	 "<br></br>"+
		 "<p>In this task, you will be presented with several choices between two monetary rewards.</p>"+
		 "<p>The choices are presented in rows. Each row presents a choice between a reward on the left, and a reward on the right.</p>"+
		 "<p>The two rewards always vary in their reward magnitude, and in their time of delivery.</p>"+
		 "<p>For example, you may be asked to choose between receiving <strong>&euro;8 today</strong> versus <strong>&euro;13 in 50 days</strong>.</p>"+
		 "<p>As you can see, one of the two rewards is delivered today, while the other reward is larger but is delivered only in 50 days.</p>"+
		 "<p>For each choice, select the reward option that you would prefer to receive.</p>"

var i1 = "<p>We are interested in your preferences, so there are no correct or incorrect answers.</p>"+
		"<p>However, if you make a choice that is unexpected or unusual, you may receive a notification asking you whether you are sure about your choice.</p>"+
		"<p>This notification does not mean you should change your choice. Instead, it asks you to double check your choice.</p>"+
		"<p>Depending on your browsers, this notification may exit your full screen. If this happens, please go back to full screen yourself.</p>"+
		 "<p>There is no time limit in this task, so take your time to make the choices.</p>"

var i2 = "<p>The choices in this task are hypothetical. However, at the end of this experiment, there will be a lottery, where you have a X chance to win one of the rewards that you chose during the task (for example, you may win &euro;13 in 50 days).</p>"+
		 "<p>Thus, please make your choices according to your true preferences.</p>"

var i3 = "<p>Are you ready to start the task?"+
	     "<p>If anything is unclear, you can go back to reread the instructions.</p>"+
		 "<p>If everything is clear, you can click on 'Next' to start the task.</p>"

//Post-gem game titrator

var i4 = "<h2>Choice task</h2>"+
	 	 "<br></br>"+
		 "<p>Finally, you will again be presented with several choices between two monetary rewards.</p>"+
		 "<p>Again, the two rewards always vary in their reward magnitude, and in their time of delivery.</p>"+
		 "<p>But pay attention: the rewards are not the same as the ones you have seen before!</p>"+
		 "<p>For each choice, select the reward option that you would prefer to receive.</p>"

var i5 = "<p>We are interested in your preferences, so there are no correct or incorrect answers.</p>"+
		"<p>However, similar as before, if you make a choice that is unexpected or unusual, you may receive a notification asking you whether you are sure about your choice.</p>"+
		"<p>This notification does not mean you should change your choice. Instead, it asks you to double check your choice.</p>"+
		 "<p>There is no time limit in this task, so take your time to make the choices.</p>"

var i6 = "<p>Similar as before, you have a X chance to win one of the rewards that you chose during the task.</p>"+
		 "<p>Thus, please make your choices according to your true preferences.</p>"
		 "<p>If you are ready to start the task, click 'Next'</p>"

