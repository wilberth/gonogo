Array.prototype.sum = Array.prototype.sum || function() {
  return this.reduce(function(sum, a) { return sum + Number(a) }, 0)
}
Array.prototype.average = Array.prototype.average || function() {
  return this.sum() / (this.length || 1)
}
function activateNext(i){
	if(i==28) return // row 28 has no next row
	// activate the next row
	document.getElementById(`n${i+2}`).disabled = false
	document.getElementById(`l${i+2}`).disabled = false
	if(i==26) return
	// if the next row is clicked, it activates the row after that
	document.getElementById(`n${i+2}`).addEventListener("change", () => activateNextWarn(i+2), false)
	if(i==24)
		document.getElementById(`l${i+2}`).addEventListener("change", () => activateNextWarn(i+2), false)
	else
		document.getElementById(`l${i+2}`).addEventListener("change", () => activateNext(i+2), false)
}
function activateNextWarn(i){
	// same as activateNext, but clicking LL option will also trigger a warning
	activateNext(i)
	document.getElementById(`l${i+2}`).addEventListener("change", () => confirm("Are you sure?"), false)
}

function initSimple(offset=0){
	// make a list of SS and LL options where people are expected to switch 
	// from LL to SS at some point where SS is large enough.
	// called on load
	let table = document.querySelector("table")
	for(let V=2; V<=28; V+=2) { // V is value in SS
		table.innerHTML += `
		<tr>
			<td class=now>&euro;${V+offset} today</td>
			<td><input id=n${V} name=r${V} type=radio ${V==2?"":"disabled"} required></td>
			<td>&nbsp;</td>
			<td><input id=l${V} name=r${V} type=radio ${V==2?"":"disabled"} required></td>
			<td>&euro;${28+offset} in 120 days</td>
		</tr>
		`
	}
	document.getElementById(`n2`).addEventListener("change", () => activateNextWarn(2), false)
	document.getElementById(`l2`).addEventListener("change", () => activateNext(2), false)
	//document.getElementById(`l28`).addEventListener("change", () => alert("Are you sure?"), false)

	//document.getElementById(`submit`).addEventListener("click", () => {
	document.getElementById(`form`).addEventListener("submit", () => {
		// calculate average switchvalue (from LL to SS) as indifferenceValue
		window.simpleChoices = []
		for(let V=2; V<=28; V+=2) { 
			window.simpleChoices.push({SS:V, LL:28, 
				choice: document.getElementById(`n${V}`).checked ? "SS" : "LL"})
		}
		let switchValue = []
		for(let V=4; V<=28; V+=2) { 
			if(document.getElementById(`n${V}`).checked && document.getElementById(`l${V-2}`).checked){
				switchValue.push(V-1)
			}
		}
		if(switchValue.length==0){
			if(document.getElementById(`n4`).checked) // all sooner
				window.indifferenceValue = 1
			else // all later
				window.indifferenceValue = 28
		} else {
			window.indifferenceValue = switchValue.average() // indifference value for LL = 28 in 120 days
		}
		console.log(`indifference value for LL = 28 in 120 days: SS = ${window.indifferenceValue}`) 
		document.getElementById("submit2").click() // the button that jsPsych is listening to
	}, false)
}
